UPDATE `Account`
LEFT JOIN xref_translation `x_ExpECM_HMIS__Victim_Service_Provider__c` ON `x_ExpECM_HMIS__Victim_Service_Provider__c`.`List Name (Global Value Set API Name)` = 'NoYesMissing' AND `x_ExpECM_HMIS__Victim_Service_Provider__c`.`Picklist Text Value` = `Account`.`ExpECM_HMIS__Victim_Service_Provider__c`
SET
`ExpECM_HMIS__Victim_Service_Provider__c` = `x_ExpECM_HMIS__Victim_Service_Provider__c`.`Integer Value`;

UPDATE `Contact`
LEFT JOIN xref_translation `x_ExpECM_HMIS__DOB_Data_Quality__c` ON `x_ExpECM_HMIS__DOB_Data_Quality__c`.`List Name (Global Value Set API Name)` = 'DOBDataQuality' AND `x_ExpECM_HMIS__DOB_Data_Quality__c`.`Picklist Text Value` = `Contact`.`ExpECM_HMIS__DOB_Data_Quality__c`
LEFT JOIN xref_translation `x_ExpECM__Ethnicity__c` ON `x_ExpECM__Ethnicity__c`.`List Name (Global Value Set API Name)` = 'Ethnicity' AND `x_ExpECM__Ethnicity__c`.`Picklist Text Value` = `Contact`.`ExpECM__Ethnicity__c`
LEFT JOIN xref_translation `x_ExpECM__Gender__c` ON `x_ExpECM__Gender__c`.`List Name (Global Value Set API Name)` = 'Gender' AND `x_ExpECM__Gender__c`.`Picklist Text Value` = `Contact`.`ExpECM__Gender__c`
LEFT JOIN xref_translation `x_ExpECM_HMIS__Name_Data_Quality__c` ON `x_ExpECM_HMIS__Name_Data_Quality__c`.`List Name (Global Value Set API Name)` = 'NameDataQuality' AND `x_ExpECM_HMIS__Name_Data_Quality__c`.`Picklist Text Value` = `Contact`.`ExpECM_HMIS__Name_Data_Quality__c`
LEFT JOIN xref_translation `x_ExpECM__Sexual_Orientation__c` ON `x_ExpECM__Sexual_Orientation__c`.`List Name (Global Value Set API Name)` = 'SexualOrientation' AND `x_ExpECM__Sexual_Orientation__c`.`Picklist Text Value` = `Contact`.`ExpECM__Sexual_Orientation__c`
LEFT JOIN xref_translation `x_ExpECM_HMIS__SSN_Data_Quality__c` ON `x_ExpECM_HMIS__SSN_Data_Quality__c`.`List Name (Global Value Set API Name)` = 'SSNDataQuality' AND `x_ExpECM_HMIS__SSN_Data_Quality__c`.`Picklist Text Value` = `Contact`.`ExpECM_HMIS__SSN_Data_Quality__c`
LEFT JOIN xref_translation `x_ExpECM__Veteran_Status__c` ON `x_ExpECM__Veteran_Status__c`.`List Name (Global Value Set API Name)` = 'NoYesReasonsforMissingData' AND `x_ExpECM__Veteran_Status__c`.`Picklist Text Value` = `Contact`.`ExpECM__Veteran_Status__c`
SET
`ExpECM_HMIS__DOB_Data_Quality__c` = `x_ExpECM_HMIS__DOB_Data_Quality__c`.`Integer Value`,
`ExpECM__Ethnicity__c` = `x_ExpECM__Ethnicity__c`.`Integer Value`,
`ExpECM__Gender__c` = `x_ExpECM__Gender__c`.`Integer Value`,
`ExpECM_HMIS__Name_Data_Quality__c` = `x_ExpECM_HMIS__Name_Data_Quality__c`.`Integer Value`,
`ExpECM__Sexual_Orientation__c` = `x_ExpECM__Sexual_Orientation__c`.`Integer Value`,
`ExpECM_HMIS__SSN_Data_Quality__c` = `x_ExpECM_HMIS__SSN_Data_Quality__c`.`Integer Value`,
`ExpECM__Veteran_Status__c` = `x_ExpECM__Veteran_Status__c`.`Integer Value`;