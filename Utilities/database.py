from Utilities import databaseConnection, mycursor



def setDatabase(dbName):
    # use sample world database
    mycursor.execute('use `' + dbName + '`;')

def showDatabases():
    # show all databases
    mycursor.execute("show databases;")
    print(mycursor.fetchall())


def simpleDbTest():



    # simple query
    mycursor.execute("SELECT * FROM city")

    myresult = mycursor.fetchall()

    for x in myresult:
        print(x)

def runQuery(queryStr):

    print('Executing query:')
    print(queryStr)

    # simple query
    mycursor.execute(queryStr)
    myresult = mycursor.fetchall()
    for x in myresult:
        print(x)

def runMultiQuery(queryStr):
    for result in databaseConnection.cmd_query_iter(queryStr):
        if 'columns' in result:
            columns = result['columns']
            rows = mycursor.get_rows()