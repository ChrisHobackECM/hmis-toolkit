from os import listdir
from os.path import isfile, join




def getQueryOrder(pathToFolder):
    onlyfiles = [f for f in listdir(pathToFolder) if isfile(join(pathToFolder, f))]

    res = {}
    for fileName in onlyfiles:
        fileOrder = fileName.split("_")[0]
        relativeFilePath = pathToFolder + "\\" + fileName
        res[int(fileOrder)] = getFileContents(relativeFilePath)

    return res




def getFileContents(filePath):
    f = open(filePath, "r")
    contents = f.read()
    return contents
