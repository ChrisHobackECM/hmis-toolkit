import mysql.connector

databaseConnection=mysql.connector.connect(
    host='localhost',
    password='admin',
    user='root'
)

if databaseConnection.is_connected():
    print('Connection Created.....')

    mycursor = databaseConnection.cursor()

else:
    print('Unable to connect')
