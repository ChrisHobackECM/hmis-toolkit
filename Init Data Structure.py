from Utilities.readFile import *
from Utilities.database import *
from Utilities import *


# show databases avaiable via this connection
showDatabases()

# connect to the database we want to load the data in
setDatabase("sl-hmis")

# get list of quries to run in their order
files = getQueryOrder("sql\init")

# run each query
for x in files:
    runMultiQuery(files[x])

